(function (Drupal, $) {
  $.fn.getUniqueSelector = function () {
    function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }

    var element = $(this);
    if (element.attr('id')) {
      return '#' + element.attr('id');
    }

    var selector = 'data-mautic-form-translation-uuid';
    if (!element.attr(selector)) {
      element.attr(selector, uuidv4());
    }

    return '[' + selector + '=' + element.attr(selector) + ']';
  };

  Object.defineProperty(String.prototype, 'hashCode', {
    value: function() {
      var hash = 5381;
      var i = this.length;

      while (i) {
        hash = (hash * 33) ^ this.charCodeAt(--i);
      }

      return hash >>> 0;
    }
  });

  Drupal.behaviors.mauticFormsFormTranslation = {
    node: null,
    translationEndpoint: null,

    attach: function (context, settings) {
      var self = this;
      var settings = settings.mautic_forms || {};
      this.node = settings.node || null;
      this.translationEndpoint = settings.translation_endpoint || null;

      if (!this.translationEndpoint) {
        return;
      }

      $('.mautic-form form', context)
        .once('mauticFormsFormTranslation')
        .each(function (_, form) {
          self.translateForm(form);
        });
    },

    translateForm: function (form) {
      var $container = $(form).parents('.mautic-form');
      var translations = {};
      var translateableElements = [
        'label',
        'option',
        'button',
        '.mauticform-errormsg',
      ];

      // Add a loading overaly.
      $container.addClass('mautic-form--loading');

      // Collect translatable strings, giving each element a unique selector for replacements.
      $(translateableElements.join(','), $(form)).each(function (_, element) {
        var selector = $(element).getUniqueSelector();
        var text = $(element).text();

        if (text !== '') {
          translations[selector] = $(element).text();
        }
      });

      var payload = JSON.stringify(translations);
      var hash = JSON.stringify(Object.values(translations)).hashCode();
      var endpoint = this.translationEndpoint
        .replace('NODE_ID', this.node)
        .replace('FORM_ID', $('[name="mauticform[formId]"]', $(form)).val())
        .replace('HASH', hash);
      var applyTranslations = function (translations) {
        // Replace untranslated text with their translated versions.
        for (var key in translations) {
          $(key).text(translations[key]);
        }

        // Remove the loading overlay.
        $container.removeClass('mautic-form--loading');
      };

      // Request translations from Drupal and apply them.
      $.get(endpoint, function (translations) {
        applyTranslations(translations);
      }).fail(function (response) {
        // Drupal returns 404 if there were no cached translations available.
        // Silently fail on any other error.
        if (response.status !== 404) {
          return;
        }

        // If there were no translations available from the cached endpoint,
        // we need to post the data up and get them.
        $.post(endpoint, payload, function (translations) {
          applyTranslations(translations);
        });
      })
    }
  };
})(Drupal, jQuery);
