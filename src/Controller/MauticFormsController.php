<?php

declare(strict_types = 1);

namespace Drupal\mautic_forms\Controller;

use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Drupal\node\NodeInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\mautic_forms\MauticApiFactory;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides a controller for mautic forms autocomplete requests.
 */
class MauticFormsController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The error message logged when we could not retrieve forms from the API.
   */
  protected const ERROR_RETRIEVING_FORMS = 'There was an error retrieving forms from the Mautic API.';

  /**
   * The cache ID template used when storing and looking up cached translated text.
   */
  protected const CACHE_ID = 'mautic_form_translation.LANGCODE.NODE_ID.FORM_ID.HASH';

  /**
   * The Mautic API factory.
   *
   * @var Drupal\mautic_forms\MauticApiFactory
   */
  protected $apiFactory;

  /**
   * The mautic_forms logger channel.
   *
   * @var Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The meesenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The default cache backend service.
   *
   * @var Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The language manager service.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructor.
   *
   * @param Drupal\mautic_forms\MauticApiFactory $apiFactory
   *   The Mautic API factory.
   * @param Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   * @param Psr\Log\LoggerInterface $logger
   *   The mautic_forms logger channel.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param Drupal\Core\Cache\CacheBackendInterface $cache
   *   The default cache backend service.
   * @param Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    MauticApiFactory $apiFactory,
    TranslationInterface $stringTranslation,
    LoggerInterface $logger,
    MessengerInterface $messenger,
    CacheBackendInterface $cache,
    LanguageManagerInterface $languageManager
  ) {
    $this->apiFactory = $apiFactory;
    $this->stringTranslation = $stringTranslation;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->cache = $cache;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('mautic_forms.api_factory'),
      $container->get('string_translation'),
      $container->get('logger.channel.mautic_forms'),
      $container->get('messenger'),
      $container->get('cache.default'),
      $container->get('language_manager')
    );
  }

  /**
   * Autocomplete handler for the Mautic Form field widget.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   The autocomplete results in JSON format.
   */
  public function autocomplete(Request $request): JsonResponse {
    $query = $request->query->get('q');

    if (empty($query)) {
      return new JsonResponse([]);
    }

    $query = Xss::filter($query);
    // The API does not handle spaces for some reason.. This replaces them with
    // the wildcard character, which gives us a more "fuzzy" search.
    $query = str_replace(' ', '%', $query);
    $forms = [];

    try {
      if ($api = $this->apiFactory->get('forms')) {
        $forms = $api->makeRequest('forms', [
          'search' => "%{$query}%",
          'published' => TRUE,
          'minimal' => TRUE,
        ])['forms'] ?? [];
      }
    }
    catch (\Exception $e) {
      $this->logger->error(static::ERROR_RETRIEVING_FORMS);

      return new JsonResponse([]);
    }

    $results = [];
    foreach ($forms as $form) {
      $results[] = [
        'value' => "${form['name']} (${form['id']})",
        'label' => $form['name'],
      ];
    }

    return new JsonResponse($results);
  }

  /**
   * Returns a JSON object of translated form text results.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param int $node_id
   *   The current node id.
   * @param int $form_id
   *   The form id of the form being translated.
   * @param int $hash
   *   The hash of the form values to be translated.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON object of translated text results.
   */
  public function formTranslation(Request $request, int $node_id, int $form_id, int $hash): JsonResponse {
    $data = json_decode($request->getContent(), TRUE);

    $translations = [];
    foreach ($data as $key => $text) {
      $translated = (string) $this->t($text);
      if ($translated === $text) {
        continue;
      }

      $translations[$key] = $translated;
    }

    $this->cache->set(
      $this->getCacheId($node_id, $form_id, $hash),
      $translations,
      CacheBackendInterface::CACHE_PERMANENT,
      ['locale']
    );

    return new JsonResponse($translations);
  }

  /**
   * Returns a JSON object of translated form text results from cache.
   *
   * @param int $node_id
   *   The current node id.
   * @param int $form_id
   *   The form id of the form being translated.
   * @param int $hash
   *   The hash of the form values to be translated.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   A JSON object of translated text results, if found. A 404 response otherwise.
   */
  public function cachedFormTranslation(int $node_id, int $form_id, int $hash): Response {
    if (!$cache = $this->cache->get($this->getCacheId($node_id, $form_id, $hash))) {
      return new Response('No cached translations found.', Response::HTTP_NOT_FOUND);
    }

    $metadata = (new CacheableMetadata())
      ->setCacheContexts(['languages'])
      ->setCacheTags($cache->tags);

    return (new CacheableJsonResponse($cache->data))
      ->addCacheableDependency($metadata);
  }

  /**
   * Generate a cache id from the static template.
   *
   * @param Drupal\node\NodeInterface $node
   *   The current node.
   * @param int $form_id
   *   The form id of the form being translated.
   * @param int $hash
   *   The content hash to use for the cache id.
   *
   * @return string
   *   The cache id.
   */
  protected function getCacheId(int $node_int, int $form_id, int $hash): string {
    return str_replace(
      ['LANGCODE', 'NODE_ID', 'FORM_ID', 'HASH'],
      [$this->languageManager->getCurrentLanguage()->getId(), $node_id, $form_id, $hash],
      static::CACHE_ID
    );
  }

}
