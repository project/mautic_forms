<?php

declare(strict_types = 1);

namespace Drupal\mautic_forms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mautic_forms\MauticApiFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A form used for configuring the Mautic Forms module.
 */
class MauticFormsSettingsForm extends ConfigFormBase {

  /**
   * The configuration key for our settings.
   */
  protected const SETTINGS = 'mautic_forms.settings';

  /**
   * The name of our form.
   */
  protected const FORM_NAME = 'mautic_forms_settings_form';

  /**
   * The Mautic API factory.
   *
   * @var Drupal\mautic_forms\MauticApiFactory
   */
  protected $apiFactory;

  /**
   * The meesenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor.
   *
   * @param Drupal\mautic_forms\MauticApiFactory $apiFactory
   *   The Mautic API factory.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    MauticApiFactory $apiFactory,
    MessengerInterface $messenger
  ) {
    $this->apiFactory = $apiFactory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('mautic_forms.api_factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $config = $this->config(static::SETTINGS);

    $form['mautic']['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Settings'),
    ];

    $form['mautic']['api']['mautic_api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mautic Username'),
      '#required' => TRUE,
      '#default_value' => $config->get('mautic.api.username'),
    ];

    $form['mautic']['api']['mautic_api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mautic Password'),
      '#required' => TRUE,
      '#default_value' => $config->get('mautic.api.password'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $config = $this->config(static::SETTINGS);
    $settings = [
      'mautic.api.username' => 'mautic_api_username',
      'mautic.api.password' => 'mautic_api_password',
    ];

    foreach ($settings as $setting => $element) {
      $config->set($setting, $form_state->getValue($element));
    }
    $config->save();

    parent::submitForm($form, $form_state);

    $api = $this->apiFactory->get('forms');
    $api->getList();

    if ($api->getResponseInfo()) {
      $this->messenger->addMessage(
        $this->t('Successfully authenticated with the Mautic API.')
      );
    }
    else {
      $this->messenger->addError(
        $this->t('There was an error authenticating with the Mautic Api. Ensure your API credentials are correct.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return static::FORM_NAME;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::SETTINGS];
  }

}
