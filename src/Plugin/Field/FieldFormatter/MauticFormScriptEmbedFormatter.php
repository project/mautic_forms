<?php

declare(strict_types = 1);

namespace Drupal\mautic_forms\Plugin\Field\FieldFormatter;

use Drupal\Core\Url;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'mautic_form_script_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "mautic_form_script_embed",
 *   label = @Translation("Mautic Form Script Embed"),
 *   field_types = {
 *     "mautic_form",
 *   },
 * )
 */
class MauticFormScriptEmbedFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager service.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(
    FieldItemListInterface $items,
    $langcode
  ): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'mautic_form_script_embed',
        '#id' => (int) $item->target_id,
      ];

      if (!$this->shouldTranslateForms()) {
        continue;
      }

      $elements[$delta]['#attached'] = [
        'library' => [
          'mautic_forms/form-translation'
        ],
        'drupalSettings' => [
          'mautic_forms' => [
            'translation_endpoint' => Url::fromRoute(
              'mautic_forms.form_translation',
              [
                'node_id' => 'NODE_ID',
                'form_id' => 'FORM_ID',
                'hash' => 'HASH',
              ]
            )->toString(),
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [$this->t('Displays the Mautic Form using the JavaScript embed method')];
  }

  /**
   * Determines wether or not forms should be translated.
   *
   * @return bool
   *   Wether or not the forms should be translated.
   */
  protected function shouldTranslateForms(): bool {
    return $this->languageManager->getCurrentLanguage()
      !== $this->languageManager->getDefaultLanguage();
  }

}
