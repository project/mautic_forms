<?php

declare(strict_types = 1);

namespace Drupal\mautic_forms\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'mautic_form' field type.
 *
 * @FieldType(
 *   id = "mautic_form",
 *   label = @Translation("Mautic Form Embed"),
 *   module = "mautic_forms",
 *   description = @Translation("A field used to embed a Mautic form"),
 *   default_widget = "mautic_form_autocomplete",
 *   default_formatter = "mautic_form_script_embed",
 * )
 */
class MauticFormItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return !$this->get('target_id')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ): array {
    $schema = [
      'columns' => [
        'target_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ],
        'value' => [
          'type' => 'text',
          'not null' => TRUE,
        ],
      ],
    ];

    if (\drupal_get_installed_schema_version('mautic_forms') < 8901) {
      $schema['columns']['cached_html'] = [
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ): array {
    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(t('Mautic form id'));
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Mautic form name'));

    if (\drupal_get_installed_schema_version('mautic_forms') < 8901) {
      $properties['cached_html'] = DataDefinition::create('string')
        ->setLabel(t('Mautic form cached html'));
    }

    return $properties;
  }

}
